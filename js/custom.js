
/***
 * ***********Validation code***************
 ******
 ***/
/***
 * ***********Validation code***************
 ******
 ***/


















function validateFormOnSubmit(contact) {
	var reason = "";
	reason += validateName(contact.name, 'name-error1');
	reason += validateEmail(contact.email, 'email-error1');
	reason += validatePhone(contact.phone, 'phone-error1');
	// reason += validateBudget(contact.budget, 'budget-error1');
	reason += validateRequirement(contact.requirement, 'requirement-error1');

	console.log(reason);
	if (reason.length > 0) {

		return false;
	} else {
		contact.submit();
	}
}

function validateHireFormOnSubmit(contact) {
	var reason = "";
	reason += validateName(contact.name, 'name-error2');
	reason += validateEmail(contact.email, 'email-error2');
	reason += validatePhone(contact.phone, 'phone-error2');
	reason += validateRequirement(contact.requirement, 'requirement-error2');

	console.log(reason);
	if (reason.length > 0) {

		return false;
	} else {
		contact.submit();
	}
}
// validate required fields
function validateName(name) {
	var error = "";
	var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
	if (name.value.length === 0) {

		$('.' + name.className).parent().addClass('errfld');
		error = "1";
	} else if (!name.value.match(alpha)) {

		$('.' + name.className).parent().addClass('errfld');
		 error = "1";
	} else {

		$('.' + name.className).parent().removeClass('errfld');
	}
	return error;
}

// validate email as required field and format
function trim(s) {
	return s.replace(/^\s+|\s+$/, '');
}

function validateEmail(email) {
	var error = "";
	var temail = trim(email.value); // value of field with whitespace trimmed off
	var emailFilter = /^[^@]+@[^@.]+\.[^@]*\w\w$/;
	var illegalChars = /[\(\)\<\>\,\;\:\\\"\[\]]/;

	if (email.value === "") {

		$('.' + email.className).parent().addClass('errfld');
		 error = "2";
	} else if (!emailFilter.test(temail)) { //test email for illegal characters

		$('.' + email.className).parent().addClass('errfld');
		error = "3";
	} else if (email.value.match(illegalChars)) {
		error = "4";

		$('.' + email.className).parent().addClass('errfld');
		email.placeholder = "Email contains invalid characters.";
	} else {

		$('.' + email.className).parent().removeClass('errfld');
	}
	return error;
}

// validate phone for required and format
function validatePhone(phone) {
	var error = "";
	var stripped = phone.value.replace(/[\(\)\.\-\ ]/g, '');

	if (phone.value === "") {

		$('.' + phone.className).parent().addClass('errfld');
		error = '6';
	} else if (isNaN(parseInt(stripped))) {
		error = "5";

		$('.' + phone.className).parent().addClass('errfld');
		phone.placeholder = "The phone number contains illegal characters.";
	} else if (stripped.length < 6) {
		error = "6";

		$('.' + phone.className).parent().addClass('errfld');
		phone.placeholder = "The phone number is too short.";
	} else {

		$('.' + phone.className).parent().removeClass('errfld');
	}
	return error;
}

function validateBudget(name) {
	var error = "";
	if (name.selectedIndex === 0) {

		$('.' + name.className).parent().addClass('errfld');
		 error = "1";
	} else {

		$('.' + name.className).parent().removeClass('errfld');
	}
	return error;
}

function validateRequirement(name) {
	var error = "";
	var alpha = /^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$/;
	if (name.value.length === 0 || name.value.length < 4) {

		$('.' + name.className).parent().addClass('errfld2');
		 error = "1";
	} else {
		//name.style.background = 'White';

		$('.' + name.className).parent().removeClass('errfld2');
	}
	return error;
} 





$('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
	nav:false,
    responsive:{
        0:{
            items:1,
            nav:false
        },
        600:{
            items:1,
            nav:false
        },
        1000:{
            items:2,
            nav:false,
            loop:false
        }
    }
});


//--------------Tabing function-------------//
	if ($('#tabing')) {
		$('#tabing li').click(function () {
			$('.content').slideUp();
			var x = $(this).attr('data-id');
			$("#" + x).slideDown();
			$('#tabing li').removeClass('active');
			$(this).addClass('active');
		});
	}
	if ($('#tectno')) {
		$('#tectno li').click(function () {
			$('.technolist').slideUp();
			var x = $(this).attr('data-id');
			$("#" + x).slideDown();
			$('#tectno li').removeClass('active');
			$(this).addClass('active');
		});
	}
	//--------------End Tabing function-------------//